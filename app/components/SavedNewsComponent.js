import React from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import StorageManager from '../data/StorageManager';
import Article from './ArticleComponent';

class SavedNewsComponent extends React.Component {
    constructor(props){
        super(props)
                    
        this.state = {
            articles: []
        }
        this.getSavedNews();
    }

    //On initialise le state
    getSavedNews(){
        StorageManager.getArticleList().then(arts => {
            this.setState({
                articles: arts
            })
        });
    }

    //Si le boutton dans le component articleComponent est appuyé, on supprime l'article du state de ce component
    unsave = (article) => {
        let newArticles = this.state.articles; 
        for(let i = 0; i < newArticles.length; i++){
            if(newArticles[i].item.url == article.url) newArticles.splice(i, 1);
        }
        this.setState({
            articles: newArticles
        })
    }


    render(){
        return (
            <View style={styles.container}>
                <FlatList 
                    data = { this.state.articles }
                    renderItem = { this._renderItem.bind(this) }
                    keyExtractor = { (item, id) => id.toString() }
                />
            </View>
        )
    }

    _renderItem = (props) => {
        return (
            <Article article={props.item} unsave={ this.unsave }/>
        )
    }
}

const styles = StyleSheet.create({
    text: {
      backgroundColor: "blue",
      color: "white",
      fontSize: 20
    },
});

export default SavedNewsComponent
