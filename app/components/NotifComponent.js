import React from 'react';
import { Button, StyleSheet, Text, View,Image, TouchableOpacity, ImageBackground } from 'react-native';
import Delete from '../../assets/delete.png'

// Composant représentant une notif dans la liste des notifications
// C'est ici que se trouve le bouton permettant la suprression (via un callback)
class NotifComponent extends React.Component {
    constructor(props){
        super(props)
    }

    render(){
        return (
            <View style={styles.notifContainer}>
                <View style={styles.infoContainer}>
                    <Text style={styles.text}>{this.props.notif.nom}</Text>
                    <Text style={styles.text}>{this.props.notif.type}</Text>
                </View>
                <TouchableOpacity style={styles.button} onPress={()=>{
                    this.props.callback.call(this, this.props.notif.id);}}>
                    <ImageBackground source={Delete} style={styles.delete}>
                    </ImageBackground>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    button: {
        height:"70%",
        marginRight: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    delete:{
        height: 30,
        width:30,
    },
    infoContainer: {
        flex: 1,
        width:'100%',
        flexDirection:'column',
        justifyContent:'flex-start',
        alignItems:'flex-start',
    },
    notifContainer: {
        width:'90%',
        flex: 1,
        flexDirection:'row',
        justifyContent:"space-between",
        alignItems:'center',
        marginTop:10,
        marginBottom:10,
        marginLeft: 40,
        marginRight: 40,
        padding: 5,
    },
    text: {
      color: "black",
      fontSize: 20,
      textAlign:'left',
      width:'100%'
    },
});

export default NotifComponent
