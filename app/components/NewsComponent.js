import React from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import NewsService from '../../services/NewService';
import Article from './ArticleComponent';

class NewsComponent extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            articles: [],
            refreshing: true
        }
        this.fetchNews();
    }

    /* On recupere les articles via le NewService, on passe le refreshing à false une fois qu'on a tout ou que ca n'a pas fonctionné */
    fetchNews(){
        var newService = new NewsService();
        newService.getNews()
        .then(articlesFetched => {
            this.setState({
                articles: articlesFetched.articles,
                refreshing: false
            });
        }).catch(() => this.setState({refreshing: false})) 
    }

    /* Pour gérer le refresh, on passe le refreshing à true et ensuite on appelle la fonction pour recuperer les articles */
    refresh(){
        this.setState({refreshing: true});
        this.fetchNews();
    }

    render(){
        return (
            <View style={styles.container}>
                <FlatList 
                    data = { this.state.articles }
                    renderItem = { _renderItem }
                    keyExtractor = { (item, id) => id.toString() }
                    refreshing = {this.state.refreshing}
                    onRefresh={this.refresh.bind(this)}
                />
            </View>
        )
    }
}

const _renderItem = (props) => {
    return (
        <Article article={props} />
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    titleBar: {
        backgroundColor: '#5ebad1',
        flexDirection: 'row',
        height: 50,
    },
    title: {
        textAlignVertical: 'center',
        color: 'white',
        fontSize: 20,
    },
    booksmark: {
        flex: 1,
        marginLeft: 5
    }
});

export default NewsComponent
