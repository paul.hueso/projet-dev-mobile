import React from 'react';
import {StyleSheet, Text, View } from 'react-native';

// Vue appelée lors du clique sur une notification
// Elle utilise les paramétres de route pour récupérer les données necessaires
class OnNotifView extends React.Component {
    constructor(props){
        super(props);
    
        this.state = {
        }
    }

    render(){
        return (
            <View style={styles.mainContainer}>
                <Text style={styles.title}>{this.props.route.params.notification.request.content.title}</Text>
                {this.props.route.params.notification.request.content.data.type == 1 && <Text style={styles.sep}>Question</Text>}
                <Text style={styles.joke}>{this.props.route.params.notification.request.content.body}</Text>
                {this.props.route.params.notification.request.content.data.type == 1 && <Text style={styles.sep}>Réponse</Text>}
                <Text style={styles.joke}>{this.props.route.params.notification.request.content.data.response}</Text>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    title:{
        marginLeft: 10,
        marginBottom: 20,
        marginTop:20,
        marginRight: 10,
        fontSize: 25,
    },
    sep:{
        width: '100%',
        marginTop: 15,
        fontSize: 20
    },
    joke:{
        marginBottom: 10,
        marginTop:5,
        fontSize: 20,
        backgroundColor: '#ffff',
        borderRadius: 15,
        padding: 20,
        width:'100%',
        borderStyle: 'solid',
        borderColor: 'grey',
        borderWidth: 1,
        overflow: 'hidden'
    },
    mainContainer:{
        margin: '5%',
        width: '90%',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
});

export default OnNotifView
