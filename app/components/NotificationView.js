import React, { useState, useEffect } from 'react';
import { Button, FlatList, StyleSheet, Text, View } from 'react-native';
import NotifComponent from './NotifComponent';
import StorageManager from '../data/StorageManager';
import { FloatingAction } from 'react-native-floating-action';

class NotificationView extends React.Component {
    constructor(props){
        super(props);
    
        this.state = {
            notifications: [],
        }

        this.isMounted = false;
    }

    componentDidMount() {
        this.isMounted = true;

        this.fetchData();

        // Listener qui ermet de refresh la page (les notifs) lors de la prise de focus
        this.willFocusSubscription = this.props.navigation.addListener(
            'focus',
            (payload) => {
                this.fetchData();
            }
          )
    }

    componentWillUnmount() {
        if (this.isMounted && this.willFocusSubscription != null)
            this.willFocusSubscription.remove()
    }

    // Récupère les données stockées en local
    fetchData(){
        StorageManager.getNotificationList().then(
            (notifsListe)=>{
                console.log(notifsListe);
                this.setState({
                    notifications: notifsListe
                })
            }
        );
    }

    render(){
        return (
            <View style={styles.mainContainer}>
                <FlatList style={styles.list} data={this.state.notifications} renderItem={this.renderItem.bind(this)} keyExtractor={item=>item.id.toString()} 
                    ItemSeparatorComponent={()=>{return( <View style={{height:1, backgroundColor:"grey", marginLeft:25, marginRight:25}}/>);}}>
                </FlatList>
                <FloatingAction actions={actions} style={styles.floatingButton} onPressItem={
                    name => {
                        switch (name) {
                            case "jk":
                                this.props.navigation.navigate("NotifsAdd",{type:"blague"});
                                break;
                            case "msg" :
                                this.props.navigation.navigate("NotifsAdd",{type:"message"});
                                break;
                            default:
                                console.log("pas de correspondance...");
                        }
                    }
                }>
                </FloatingAction>
            </View>
        )
    }

    // Supprime une notification
    async deleteNotif(id){
        // Supprime localement
        await StorageManager.deleteNotification(id);
        // Met a jour la vue
        await StorageManager.getNotificationList().then(
            (notifsListe)=>{
                this.setState({
                    notifications: notifsListe
                })
            }
        );
    }

    renderItem({item, index}){
        return (
            <NotifComponent notif={item} callback={this.deleteNotif.bind(this)}></NotifComponent>
        );
    };
}

const actions = [
    {
        text: "Blague",
        icon: require("../../assets/joke.png"),
        name: "jk",
        position: 2
    },
    {
        text: "Message",
        icon: require("../../assets/message.png"),
        name: "msg",
        position: 1
    }
]

const styles = StyleSheet.create({
    floatingButton: {
        position: 'absolute',
    },
    list:{
        width: '100%'
    },
    mainContainer:{
        width: '100%',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    title: {
      backgroundColor: "#0ACDFF",
      padding: 10,
      width: "100%",
      color: "white",
      fontSize: 30,
      textAlign: 'center'
    },
});

export default NotificationView
