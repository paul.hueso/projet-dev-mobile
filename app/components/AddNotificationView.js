import React from 'react';
import { StyleSheet, Text, TextInput, View, TouchableOpacity, Keyboard, Platform} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import DateTimePicker from '@react-native-community/datetimepicker';
import StorageManager from '../data/StorageManager';
import NotificationMessage from '../data/NotifMessage';
import NotificationBlague from '../data/NotifBlague';
import * as Notifications from 'expo-notifications';
import fetch from 'node-fetch';


// Vue permettant d'ajouter une notification, elle prend en argumnent le type de notification voulue 
// et change l'interface en fonction
class AddNotificationView extends React.Component {
    constructor(props){
        super(props)
                    
        /* REMEMBER USE this.setState for update THIS  */
        this.state = {
            text: 'Hello from',
            // Picker open ?
            open: false,
            title: null,
            // Valeur recursivité
            value: '0',
            items: [
                {label: 'Tous les jours', value: '0'},
                {label: 'Toutes les semaines', value: '1'},
                {label: 'Tous les ans', value: '2'}
            ],
            // Valeur du picker de date
            date: new Date(),
            // Valeur du picker heure et minte
            hour: new Date(),
            // Message de l'utilisateur si le type d'interface le permet
            message : "",
            // Permet de cacher des élément de l'interface pour avoir de lespace lors de l'entrée
            // d'un texte dans le multiline text input
            hidePickers : false
        };
    }

    componentDidMount() {

        registerForPushNotificationsAsync().catch((e)=>console.log(e));

        // Au clique sur une notification cet événement sera lancé : lancer la page donnant les réponses
        Notifications.addNotificationResponseReceivedListener(response => {
            console.log(response);
            this.props.navigation.navigate("OnNotif",response);
        });
    }

    // Set la value de recursivité
    setValue(callback) {
        this.setState(state => ({
            value: callback(state.value),
        }));
    }

    setItems(callback) {
        this.setState(state => ({
            items: callback(state.items)
        }));
    } 

    render(){
        const {open, value, items} = this.state;

        return (
            <View style={styles.container}>
                <Text style={styles.text}>{this.renderTitle()}</Text>
                <View style={styles.divider}></View>
                    {
                        !this.state.hidePickers && (<TextInput
                        style={styles.textInputTitle}
                        value={this.state.title}
                        onChangeText={text=>this.setState({title:text})}
                        multiline={false}   
                        onSubmitEditing={()=>{
                            Keyboard.dismiss();
                        }}
                        placeholder={"Nom de la notification ..."}
                    />)}
                    {
                        !this.state.hidePickers && (<DropDownPicker
                        style={styles.dropDownPicker}
                        containerStyle={styles.selectContainer}
                        dropDownContainerStyle={styles.dropDownContainer}
                        selectedItemContainerStyle={{
                            flex:1,
                            flexDirection: "row",
                        }}
                        selectedItemLabelStyle={{
                            fontWeight: "bold",
                            color: '#1253bc',
                        }}
                        listItemLabelStyle={{
                            padding: 6,
                        }}
                        open={open}
                        value={value}
                        items={items}
                        setOpen={(open)=>this.setState({open})}
                        setValue={this.setValue.bind(this)}
                        setItems={this.setItems.bind(this)}
                        itemSeparator={true}
                    />)}
                    { !this.state.hidePickers && (<View style={styles.hourform}>
                        <Text style={styles.title}>Choix de l'heure</Text>
                        <DateTimePicker
                            style={styles.picker}
                            testID="dateTimePicker"
                            value={this.state.hour}
                            mode={'time'}
                            is24Hour={true}
                            display="default"
                            onChange={(event, selectedDate)=>{
                                const currentDate = selectedDate;
                                this.setState({hour : currentDate});
                            }}
                        />
                    </View>)}
                    { !this.state.hidePickers && (<View style={styles.hourform}>
                        <Text style={styles.title}>Choix de la date</Text>
                        <DateTimePicker
                            style={styles.picker}
                            testID="dateTimePicker"
                            value={this.state.date}
                            mode={'date'}
                            is24Hour={true}
                            display="default"
                            onChange={(event, selectedDate)=>{
                                const currentDate = selectedDate;
                                this.setState({date : currentDate});
                            }}
                        />
                    </View>)}
                    {
                        this.props.route.params.type === "message" && ( <TextInput
                            style={styles.textInput}
                            value={this.state.message}
                            onChangeText={text=>this.setState({message:text})}
                            multiline={true}   
                            onSubmitEditing={()=>{
                                this.setState({hidePickers: false});
                                Keyboard.dismiss();
                            }}
                            placeholder={"Entrez votre message ici ..."}
                            onFocus={()=>this.setState({hidePickers: true})}
                            maxLength={100}
                    />)
                    }
                <TouchableOpacity onPress={this.addNotification.bind(this)} style={styles.button}><Text style={styles.textButton}>Ajouter la Notification</Text></TouchableOpacity>
            </View>
        )
    }

    // Titre différent selon type de la vue
    renderTitle(){
        switch(this.props.route.params.type){
            case "message":
                return "Notification message personalisé";
            case "blague":
                return "Notification blague";
            default:
                return "Problème de route";    
        }
    }

    // Ajout une notification en récupérant les valeurs des pickers et text input 
    async addNotification(notif){
        var realDate = this.state.date;
        var hour = this.state.hour.getHours();
        var min =this.state.hour.getMinutes();
        realDate.setHours(hour);
        realDate.setMinutes(min);

        if (this.state.title != null){
            // Si message on créer un objet message
            if (this.props.route.params.type === "message"){
                // On enregistre l'id de notification pour pouvoir la supprimer plus tard si besoin
                var token = await this.schedulePushNotificationMessage(realDate,this.state.title, this.state.message, this.state.value);
                // On créer concrétement la notification
                await StorageManager.addNotification(new NotificationMessage(this.state.title,realDate,this.state.value,this.state.message,token));
            // Sinon on créer un objet blague
            }else{
                var token =  await this.schedulePushNotificationBlague(realDate,this.state.title, this.state.value);
                await StorageManager.addNotification(new NotificationBlague(this.state.title,realDate,this.state.value, token));
            }
            this.props.navigation.goBack();
        }else{
            // ona a au moins besoin d'un titre pour créer la notif
            alert("Veuillez rentrez un titre correct");
        }
    }

    // Fonction pour enregistrer une notif message en lui donnant un corps qui sera géré par expo
    async schedulePushNotificationMessage(date, title, message, recursivite) {
        return await Notifications.scheduleNotificationAsync({
            content: {
                title: title,
                body: message,
                data: { response: 'Message correctement transmis',type: 0 },
            },
            trigger: getTrigger(recursivite,date),
        });
    }

    // Fonction pour enregistrer une notif blague
    async schedulePushNotificationBlague(date, title, recursivite) {
        // On récupère dans un premier temps une blague via l'api de blague
        return fetch('https://www.blagues-api.fr/api/type/dev/random', {
            headers: {'Authorization': `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiMjMzNjIyNjkzNDU3NDI4NDkxIiwibGltaXQiOjEwMCwia2V5IjoiN1dDamxoTXJycmhnNmx1RGxQZUwxRnB6Z0U3OWx4eTVOck1UOHZDNHlYaUV2ZHN4Z0YiLCJjcmVhdGVkX2F0IjoiMjAyMS0wNS0xN1QxNjoyMDozMyswMDowMCIsImlhdCI6MTYyMTI2ODQzM30.wZAlQdBVECpTpI97Cmf8_PCh8YngbbP2PhvcWAMaNYA`}
        }).then(response => response.json())
        // On créer la notif avec la réponse parsée de l'api
        .then(async (data) => {
            return await Notifications.scheduleNotificationAsync({
                content: {
                    title: title,
                    body: data.joke,
                    data: {response: data.answer, type: 1},
                },
                trigger: getTrigger(recursivite,date),
            });
        })
    }
}

// En fonction de l'os on programme différemment la récursivité
const getTrigger = (type, date)=>{
    if (Platform.OS === 'ios'){
        // Avec "repeat"
        switch(type){
            case '0':
                return {
                    hour: date.getHours(),
                    minute: date.getMinutes() + 1,
                    repeats: true
                };
            case '1':
                return {
                    weekday: date.getDay(),
                    hour: date.getHours(),
                    minute: date.getMinutes(),
                    repeats: true
                };
            case '2':
                return {
                    month: date.getMonth(),
                    weekday: date.getDay(),
                    hour: date.getHours(),
                    minute: date.getMinutes(),
                    repeats: true
                };
        }
    }
    if (Platform.OS === 'android'){
        // Avec un "type"
        switch(type){
            case '0':
                return {
                    type: 'daily',
                    hour: date.getHours(),
                    minute: date.getMinutes(),
                };
            case '1':
                return {
                    type: 'weekly',
                    weekday: date.getDay(),
                    hour: date.getHours(),
                    minute: date.getMinutes(),
                };
            case '2':
                return {
                    type: 'yearly',
                    day: date.getDay(),
                    month: date.getMonth(),
                    hour: date.getHours(),
                    minute: date.getMinutes(),
                };
        }
    }
}

// Fonction préfaite par expo pour demander les droits / activer notifications
async function registerForPushNotificationsAsync() {
    let token;
    if (Constants.isDevice) {
      const { status: existingStatus } = await Notifications.getPermissionsAsync();
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Notifications.requestPermissionsAsync();
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        alert('Failed to get push token for push notification!');
        return;
      }
      token = (await Notifications.getExpoPushTokenAsync()).data;
      console.log(token);
    } else {
      alert('Must use physical device for Push Notifications');
    }
  
    if (Platform.OS === 'android') {
      Notifications.setNotificationChannelAsync('default', {
        name: 'default',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: '#FF231F7C',
      });
    }
  
    return token;
  }

const styles = StyleSheet.create({
    selectContainer:{
        height:80,
    },
    picker:{
        width: "100%",
        marginTop: 10
    },
    title:{
        fontSize: 20,
        marginBottom: 10
    },
    button: {
        width:"90%",
        marginRight:40,
        marginLeft:40,
        marginTop:10,
        marginBottom: 20,
        paddingTop:10,
        paddingBottom:10,
        backgroundColor:'#ffff',
    },
    textButton:{
        width: "100%",
        textAlign: "center",
        fontSize: 20,
        color: "#1253bc"
    },
    textInput: {
        width: "90%",
        backgroundColor:'#ffff',
        padding:15,
        fontSize:20,
        marginTop: 10,
        flex:1
    },
    textInputTitle:{
        width: "90%",
        backgroundColor:'#ffff',
        padding:15,
        fontSize:20,
        marginTop: 10,
        flex:0.3
    },
    hourform: {
        backgroundColor: '#ffff',
        margin: 10,
        paddingTop: 0,
        paddingLeft: 30,
        paddingRight: 30,
        width: '90%',
        flex:1,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    dropDownContainer: {
        marginLeft: "5%",
        marginRight: "5%",
        marginTop: 15,
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 30,
        paddingRight: 30,
        borderRadius: 0,
        width: '90%',
    },
    dropDownPicker: {
        width: '90%',
        backgroundColor: '#ffff',
        marginLeft: "5%",
        marginRight: "5%",
        marginTop: 15,
        marginBottom: 15,
        paddingTop: 15,
        paddingBottom: 15,
        paddingLeft: 30,
        paddingRight: 30,
        flex: 1,
        flexDirection:'row',
        alignItems: 'center',
    },
    container: {
        flex: 1,
        alignItems: "center",
        width: "100%",
        justifyContent:"flex-start"
    },
    text: {
        marginTop: 10,
        fontSize: 20
    },
    divider: {
        height: 2,
        margin: 15,
        width: '90%',
        backgroundColor: "grey"
    }
});

export default AddNotificationView
