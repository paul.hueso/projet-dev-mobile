import moment from 'moment';
import React from 'react';
import { StyleSheet, Text, View, Image, Linking, Alert, Share, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import StorageManager from '../data/StorageManager';


class ArticleComponent extends React.Component {
    constructor(props){
        super(props)
                    
        this.state = {
            saved: null
        }

        /* On verifie si l'article n'est pas deja enregistré */
        StorageManager.alreadyRegistered(this.props.article).then( (savedArt) => {
            this.setState({saved: savedArt});
        });
    }

    /* Supprimer on ajouter un article aux articles enregistrés */
    onSave(){
        if(!this.state.saved) {
            StorageManager.addArticle(this.props.article)
        }
        else {
            StorageManager.removeArticle(this.props.article)
        }
        this.setState({ saved: !this.state.saved })
    }

    /* Partager un article en envoyant l'url */
    onShare(){
        Share.share({
            message: this.props.article.item.url,
        })
    }

    render(){
        const article = this.props.article.item;
        const time = moment(article.publishedAt).fromNow(); //Depuis combien de temps l'article est posté
        return (
            <TouchableOpacity style={styles.container} onPress={() => Linking.openURL(article.url)} activeOpacity={1}>
                <Image 
                    style={styles.image}
                    source={{uri: article.urlToImage}}
                />
                <View style={styles.buttonContainer}>
                    <Icon.Button 
                        name="share-social" 
                        size={30} 
                        backgroundColor={'transparent'} 
                        style={styles.buttonShare} 
                        onPress={() => this.onShare()} 
                    />
                    <Icon.Button
                        name={this.state.saved ? "bookmark" : "bookmark-outline"} 
                        size={30} 
                        backgroundColor={'transparent'} 
                        style={styles.buttonBookmark} 
                        onPress={() => {
                            this.onSave();
                            if(this.props.unsave != undefined){
                                this.props.unsave(article);
                            }
                        }}
                    />
                </View>

                <View style={styles.textContainer}>

                    <Text style={styles.title}>{article.title}</Text>
                    <Text style={styles.description}>{article.description || '[En savoir plus...]'}</Text>
                    
                    <View style={styles.suppInfoContainer}>
                        <Text style={styles.source}>{article.source.name}</Text>
                        <Text style={styles.time}>{time}</Text>
                    </View>

                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 50,
        margin: 15,
        height: 410,
        borderWidth: 1,
        borderColor: '#D1D8DE',
        borderRadius : 10,

    },
    image: {
        flex: 1,
        alignSelf: 'stretch'
    },
    textContainer: {
        flex: 1,
        padding: 8
    },
    title: {
        fontWeight: 'bold',
        fontSize: 16
    },
    description: {
        flex: 1,
        marginBottom: 2
    },
    suppInfoContainer: {
        borderTopWidth: 1,
        borderTopColor: '#D1D8DE',
        fontSize: 20,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    source: {
        color: "grey",
        fontStyle: 'italic',
        fontSize: 12
    },
    time: {
        color: "grey",
        fontStyle: 'italic',
        fontSize: 12
    },
    buttonContainer: {
        position: 'absolute',
        top: 0,
        width: "100%",
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    buttonBookmark: {
        marginBottom: -5,
        marginRight: -10,
        marginLeft: -5 ,
    },
    buttonShare: {
        marginBottom: -5,
        marginRight: -10,
        marginLeft: -5    
    }

});

export default ArticleComponent
