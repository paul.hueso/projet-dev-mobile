import React, {Component} from 'react'
import MapView, { Circle, Marker, PROVIDER_GOOGLE } from 'react-native-maps'
import { StyleSheet, Dimensions, Button, View, Text, TextInput, TouchableOpacity } from 'react-native'
import * as Location from 'expo-location';

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width


class MapComponent extends Component {

  constructor(props){
      super(props)
                  
      /* REMEMBER USE this.setState for update THIS  */
      this.state = {
          theme: false, //toggle theme  /// Permet de faire le switch entre le dark mod et le mod normal de la map
          userLocation: {  //user coordonate  /// Stock les coordonées de l'utilisteur via le GPS
            "coordinate": {
              latitude: 0, 
              longitude: 0,
              latitudeDelta: 0.0,
              longitudeDelta: 0.0
            }
          },
          location: {  //coordonate of the center of the circle /// Stock les coordonées du centre du cercle via le GPS
            "coordinate": {
              latitude: 0, 
              longitude: 0,
              latitudeDelta: 0.0,
              longitudeDelta: 0.0
            }
          },
          rayonCercle : 1000,  //rayon of the circle /// Stock le rayon du cercle
          distance : 0,  //distance between the center of the circle and the userLocation (update each time the userlocation is update)  /// Stock la distance entre la position de l'utilisateur et le centre du cercle (mise a jour a chaque changement de position de l'utilisateur)
          distanceInput: "", // Stock l'input /// Stock la distance saisie par l'utilisateur
          alreadyAlert: false, //give only one notification when we go outside the circle /// Permet de donner la notif qu'une seul fois lorsqu'on est sorti du cercle
      }
  }


  componentDidMount() { // Quand l'app est monté on demande la permission d'avoir la position
    this._getPermissionLocation();
  }


  async _getPermissionLocation() { //Permet de demander les permissions à l'utilisateur
    let  {status} = await Location.requestForegroundPermissionsAsync();
    if (status !== 'granted') {
      console.log('Permission to access location was denied');
    }
  }



  setPosition(){  //set la position de l'utilisateur dans le state
    this.setState({ location : this.state.userLocation });
    /* // Test de la défini des state
    console.log(this.state.location);
    console.log(this.state.userLocation);
    */
  }


  calculDistanceEtVerif(){
    //Méthode avec la formule de haversine qui calcul la distance entre 2 points sur une sphere/globe
    //Ensuite cette méthode vérifie si l'utilisateur est dans le cercle et affiche en conséquence 1 seul alerte quand il en sort
    /* Pour la calcul de distance dans 2 point (longitude et latitude) 
     * https://fr.wikipedia.org/wiki/Formule_de_haversine
     */

    let long1 = this.state.location.coordinate.longitude * Math.PI/180; //en radian
    let long2 = this.state.userLocation.coordinate.longitude * Math.PI/180; //en radian
    let lat1 = this.state.location.coordinate.latitude * Math.PI/180; //en radian
    let lat2 = this.state.userLocation.coordinate.latitude * Math.PI/180; //en radian


    let a = Math.pow(Math.sin((lat2-lat1)/2),2)+Math.cos(lat1)*Math.cos(lat2)*Math.pow(Math.sin((long2-long1)/2),2);
    let c = 2 * Math.asin(Math.sqrt(a));

    let resultat = (6371 * c) ; //6371 le rayon de la terre en km

    this.setState({ distance : resultat },() => { //résultat en kilomètre
      
        //ICI on verif APRES AVOIR DEFINIE LA DISTANCE DANS LE SETSATE
        if ( this.state.location.coordinate.longitude != 0 & this.state.location.coordinate.latitude != 0){ //Si la zone n'est pas à l'endroit initial (donc que l'utilisateur ne l'a pas définie)
          if( this.state.distance > this.state.rayonCercle/1000){ //Si la distance est supérieur au rayon du cercle donc on en est sorti
            if (!this.state.alreadyAlert){
              alert("Tu es sorti de la zone");
              this.setState({alreadyAlert:true});
            }
          }else{
            this.setState({alreadyAlert:false})
          }
        }

    }); 
    
  }



	render(){
		return(
      <View style={{flex:1, width: "100%", height: "100%"}}>
          <TextInput
            value={this.state.distanceInput}
            textAlign={"center"}
            onChangeText={(input) => { if( (input=="") ) {this.setState({rayonCercle : 1000 , distanceInput: input}) }else{ if(!isNaN(input) ){ this.setState({rayonCercle : parseInt(input) , distanceInput: input});}}}}
            placeholder={'Rayon du cercle (en Mètre)'}
            style={styles.input}
          />
          <TouchableOpacity style={styles.button} onPress={ this.setPosition.bind(this) } ><Text style={styles.text} >Centrer sur moi</Text></TouchableOpacity>
          

          <MapView
              provider={PROVIDER_GOOGLE}
              style={styles.map}
              customMapStyle={ this.state.theme ? DarkMapStyle : [] }
              loadingEnabled={true}

              showsUserLocation = {true}
              showsMyLocationButton = {true}
              userLocationUpdateInterval = {10000}
              followsUserLocation = {false}
              
              onUserLocationChange = {event => {this.setState({userLocation : event.nativeEvent},() => { this.calculDistanceEtVerif(); });   } } //{event => {console.log(event.nativeEvent.coordinate);} } 
              

              ref = {ref => this.map = ref}
              initialRegion={{
                latitude: 50.32912402536973, 
                longitude: 3.514403057736026,
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121
              }} 
          >

            <Marker
                coordinate={this.state.location.coordinate}
                title={"Valenciennes"}
                description={"Centre Valenciennes"}
                draggable
                onDrag={event => {this.setState({location : event.nativeEvent})}}
            ></Marker>
            <Circle
              center={this.state.location.coordinate}
              radius={this.state.rayonCercle}
              strokeColor={"#000"}
              fillColor={"rgba(20,20,200,0.3)"}
            ></Circle>


          </MapView>

          <TouchableOpacity style={styles.button} onPress={ this.toggleTheme.bind(this) } ><Text style={styles.text} >MODE SOMBRE</Text></TouchableOpacity>

      </View>
            
		);
	}


  
  toggleTheme(){ //toggle entre le drak mode et le mode normal avec le setState
    if(this.state.theme){
        this.setState({ 
            theme:false
        });
    }else{
        this.setState({ 
            theme:true
        });
    }
  }
  

}


// Ici le style de certains composants
const styles = StyleSheet.create({ 
    input: {
      padding : 10,
      backgroundColor: "#FFFFFF",
      width,
      height : 50,
      fontSize:20
    },
    text: {
      fontSize: 15,
      height:"100%",
      textAlign : "center",
      justifyContent: 'center',
      color : "#FFFFFF",
      textAlignVertical: "center"
    },
    map: {
        "height": "100%",
        width,
        flex:7
    },
    button: {
      padding:10,
      width,
      backgroundColor : "#2196F3",
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
      height : 40
    }
  })

//C'est le dark mode de la map
const DarkMapStyle = [
    {
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#242f3e"
        }
      ]
    },
    {
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#746855"
        }
      ]
    },
    {
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#242f3e"
        }
      ]
    },
    {
      "featureType": "administrative.locality",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#d59563"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#d59563"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#263c3f"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#6b9a76"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#38414e"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#212a37"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#9ca5b3"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#746855"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#1f2835"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#f3d19c"
        }
      ]
    },
    {
      "featureType": "transit",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#2f3948"
        }
      ]
    },
    {
      "featureType": "transit.station",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#d59563"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#17263c"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#515c6d"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#17263c"
        }
      ]
    }
  ]



export default MapComponent