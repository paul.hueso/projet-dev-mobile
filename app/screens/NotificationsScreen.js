import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Navigator from '../navigators/StackNavigator';

class NotificationsScreen extends React.Component {
    constructor(props){
        super(props)
                    
        /* REMEMBER USE this.setState for update THIS  */
        this.state = {
            text: 'Hello from'
        }
    }

    render(){
        return (
            <Navigator style={styles.nav}></Navigator>
        )
    }
}

const styles = StyleSheet.create({
    nav: {
      backgroundColor: "blue",
      color: "white",
      fontSize: 20,
      width: "100%",
    },
});

export default NotificationsScreen
