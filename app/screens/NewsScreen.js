import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import NewsComponent from '../components/NewsComponent';

class NewsScreen extends React.Component {
    constructor(props){
        super(props)

    }

    render(){
        return (
            <NewsComponent style={styles.container}/>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

export default NewsScreen
