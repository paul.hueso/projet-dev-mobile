import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import SavedNewsComponent from '../components/SavedNewsComponent';

class SavedNewsScreen extends React.Component {
    constructor(props){
        super(props)
    }

    render(){
        return (
            <SavedNewsComponent />
        )
    }
}

const styles = StyleSheet.create({
    text: {
      backgroundColor: "blue",
      color: "white",
      fontSize: 20
    },
});

export default SavedNewsScreen
