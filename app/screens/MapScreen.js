import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import MapCompoent from '../components/MapComponent';
import {createStackNavigator} from '@react-navigation/stack'

const Stack = createStackNavigator();

class MapScreen extends React.Component {
    constructor(props){
        super(props)
                    
        /* REMEMBER USE this.setState for update THIS  */
        this.state = {
            text: 'Hello from'
        }
    }

    render(){
        return (
            <Stack.Navigator>
                <Stack.Screen name="NotifsListe" component={MapCompoent} options={getHeader("Ma carte")}/>
            </Stack.Navigator>
        )
    }
}

function getHeader(headerName){
    return {
        title: headerName,
        headerStyle: {
            backgroundColor: '#1253bc',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };
}

const styles = StyleSheet.create({
    text: {
      backgroundColor: "blue",
      color: "white",
      fontSize: 20
    },
});

export default MapScreen
