import React from 'react';
import { StyleSheet} from 'react-native';
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
import NotificationView from '../components/NotificationView';
import AddNotificationView from '../components/AddNotificationView';
import OnNotifView from '../components/OnNotifView';

const Stack = createStackNavigator();

class Navigator extends React.Component {

    constructor(props){
        super(props)
                    
        /* REMEMBER USE this.setState for update THIS  */
        this.state = {
            text: 'Hello from'
        }
    }

    render(){
        return (
            <Stack.Navigator>
                <Stack.Screen name="NotifsListe" component={NotificationView} options={getHeader("Mes Notifications")}/>
                <Stack.Screen name="NotifsAdd" component={AddNotificationView} options={getHeader('Ajouter une notification')}/>
                <Stack.Screen name="OnNotif" component={OnNotifView} options={getHeader('Details notification')}/>
            </Stack.Navigator>
        )
    }
}

function getHeader(headerName){
    return {
        title: headerName,
        headerStyle: {
            backgroundColor: '#1253bc',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };
}

const styles = StyleSheet.create({
    text: {
      backgroundColor: "blue",
      color: "white",
      fontSize: 20
    },
});

export default Navigator
