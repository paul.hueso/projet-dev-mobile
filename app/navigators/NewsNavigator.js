import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import NewsScreen from '../screens/NewsScreen';
import SavedNewsScreen from '../screens/SavedNewsScreen';
import { NavigationContainer, NavigationHelpersContext } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/Ionicons';


class NewsNavigator extends React.Component {
    constructor(props){
        super(props)
    }
    
    Stack = createStackNavigator();

    render(){
        return (
            <this.Stack.Navigator initialRouteName="NewsScreen">
                <this.Stack.Screen 
                    name="NewsScreen" 
                    component={NewsScreen} 
                    options={({ navigation }) => ({
                        headerTitle: "Articles Covid",
                        headerLeft: () => (
                            <Icon.Button
                                style={styles.booksmark} 
                                name={"md-bookmarks-outline"}
                                backgroundColor={'transparent'}
                                size={25}
                                onPress={() => navigation.navigate('SavedNewsScreen')}
                            />
                        ),
                        headerStyle: {
                            backgroundColor: '#1253bc',
                        },
                        headerTintColor: 'white',
                        headerTitleAlign: 'center'
                    })}
                />
                <this.Stack.Screen 
                    name="SavedNewsScreen" 
                    component={SavedNewsScreen} 
                    options={() => ({
                        headerTitle: "Articles sauvegardés",
                        headerStyle: {
                            backgroundColor: '#1253bc',
                        },
                        headerTintColor: 'white',
                        headerTitleAlign: 'center'  
                        })}  
                />
            </this.Stack.Navigator>
        )
    }
}

const styles = StyleSheet.create({
    text: {
      backgroundColor: "blue",
      color: "white",
      fontSize: 20
    },
});

export default NewsNavigator
