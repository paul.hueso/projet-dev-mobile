import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MapScreen from '../screens/MapScreen';
import NotifNavigator from './StackNavigator';
import NewsNavigator from './NewsNavigator';
import NotificationsScreen from '../screens/NotificationsScreen';

const Tab = createBottomTabNavigator();

class TabNavigator extends React.Component {

    constructor(props){
        super(props)
                    
        /* REMEMBER USE this.setState for update THIS  */
        this.state = {
            text: 'Hello from'
        }
    }

    render(){
        return (
            <NavigationContainer>
                <Tab.Navigator
                        screenOptions={({ route }) => ({
                            tabBarIcon: ({ focused, color, size }) => {
                              let iconName;
                  
                              if (route.name === 'Notifications') {
                                iconName = focused
                                  ? 'notifications'
                                  : 'notifications-outline';
                              } else if (route.name === 'Map') {
                                iconName = focused
                                  ? 'map'
                                  : 'map-outline';
                              } else if (route.name === 'News') {
                                iconName = focused ? 'newspaper' : 'newspaper-outline';
                              }
                  
                              return <Ionicons name={iconName} size={size} color={color} />;
                            },
                          })}
                          tabBarOptions={{
                            activeTintColor: '#1253bc',
                            inactiveTintColor: 'gray',
                          }}    
                >
                    <Tab.Screen name="Map" component={MapScreen} />
                    <Tab.Screen name="Notifications" component={NotificationsScreen} />
                    <Tab.Screen name="News" component={NewsNavigator} />
                </Tab.Navigator>
            </NavigationContainer>
        )
    }
}

export default TabNavigator
