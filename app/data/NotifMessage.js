import Notification from './Notif';

class NotificationMessage extends Notification{

    constructor(nom, date, recursivite, message, expoId){
        super(nom,date,recursivite, expoId)
        this.type = 'message';
        this.message = message;
    }
}

export default NotificationMessage