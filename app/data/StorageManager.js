import Storage from 'react-native-storage';
import { AsyncStorage } from 'react-native';
import * as Notifications from 'expo-notifications';

class StorageManager {

    static clearAll(){
        storage.clearMap();
        storage.remove({key: 'isInit'})
        storage.remove({key: 'cptIdNotifs'})
        storage.remove({key: 'cptIdArticle'})   
    }

    // Permet d'initialiser les compteurs d'ids lors de la première ouverture d'application
    static  init(){
        return new Promise( (resolve) => {
            storage.load({
                key: 'isInit'
            }).then(resolve())
            .catch(err => {
                if(err.name == 'NotFoundError'){
                    storage.save({
                        key: 'isInit',
                        data: true
                    });
                    storage.save({
                        key: 'cptIdNotifs',
                        data: 0
                    });
                    storage.save({
                        key: 'cptIdArticle',
                        data: 0
                    });
                    resolve();
                }
            });
        });
    }

    // Ajoute une notification en incrémentant ensuite le compteur
    static async addNotification(notif){
        await StorageManager.init();
        var cptId = await storage.load({
            key: 'cptIdNotifs'
        });
        notif.id = cptId++;
        await storage.save({
            key: 'cptIdNotifs',
            data: cptId,
        });
        await storage.save({
            key: 'notif',
            id: notif.id,
            data: notif,
        });
    }

    // Enleve une notification en prenant soin de l'enlever également du système de notif d'expo
    // via l'id récupéré lors de la création de la notif
    static async deleteNotification(idNotif){
        await StorageManager.init();
        notif = await storage.load({
            key: "notif",
            id: idNotif
        });
        await Notifications.cancelScheduledNotificationAsync(notif.expoId);
        await storage.remove({
            key: "notif",
            id: idNotif
        });
    }

    // La liste des notifications
    static async getNotificationList(){
        await StorageManager.init();
        return storage.getAllDataForKey('notif');
    }

    static async addArticle(article){
        await StorageManager.init();
        storage.load({
            key: 'isInit'
        }).then(a => console.log(a));

        storage.load({key: 'cptIdArticle'})
        storage.save({
            key: 'article',
            id: 1,
            data: article
        }).then(this.getArticleList)
        .catch(err => console.log(err.message));

    }

    //ajoute un article à la clé "article" + incremente le compteur
    static async addArticle(article){
        await StorageManager.init(); 
        var cptId = await storage.load({
            key: 'cptIdArticle'
        });
        await storage.save({
            key: 'article',
            id: cptId,
            data: article,
        });
        cptId++;
        await storage.save({
            key: 'cptIdArticle',
            data: cptId,
        });
    }

    //retire un article en fonction de son url qui est unique
    static async removeArticle(article){
        storage.getAllDataForKey('article')
        .then(liste => {
            for(i = 0; i < liste.length; i++){
                if(article.item.url == liste[i].item.url) {
                    let indice = i; //On sauvegarde l'indice sinon, une fois le .getIdsForKey commencé, le i aura ete incrémenté
                    storage.getIdsForKey('article') //On recupere l'id de l'objet à partir de sa position dans la liste
                    .then((artListe) => {
                        storage.remove({
                            key: 'article',
                            id: artListe[indice]
                        });
                    });
                }
            }
        })
    }

    //recupere la liste des articles
    static getArticleList(){
        StorageManager.init();
        return new Promise((resolve, reject) => {
            storage.getAllDataForKey('article').then(article => {
                if(article.length == 0) { console.log("Liste vide"); resolve(false);}
                else {
                    resolve(article);
                }
            })
        });
    }

    //verifie si un article est deja present dans la bdd
    static alreadyRegistered(article){
        return new Promise((resolve, reject) => {
            storage.getAllDataForKey('article')
            .then(liste => {
                for(i = 0; i < liste.length; i++){
                    if(article.item.url == liste[i].item.url) {
                        resolve(true);
                    }
                }
                resolve(false);
            })
        });
    }
}

const storage = new Storage({
    // maximum capacity, default 1000 key-ids
    size: 1000,
  
    // Use AsyncStorage for RN apps, or window.localStorage for web apps.
    // If storageBackend is not set, data will be lost after reload.
    storageBackend: AsyncStorage, // for web: window.localStorage
  
    // expire time, default: 1 day (1000 * 3600 * 24 milliseconds).
    // can be null, which means never expire.
    defaultExpires: null,
  
    // cache data in the memory. default is true.
    enableCache: false,
  
    // if data was not found in storage or expired data was found,
    // the corresponding sync method will be invoked returning
    // the latest data.
    sync: {
      // we'll talk about the details later.
    }
});

export default StorageManager
