import Notification from './Notif';

class NotificationBlague extends Notification{

    constructor(nom, date, recursivite, expoId){
        super(nom, date,recursivite, expoId)
        this.type = 'blague';
    }

}

export default NotificationBlague