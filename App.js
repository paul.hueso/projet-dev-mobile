import React from 'react';
import { StyleSheet, View } from 'react-native';
import TabNavigator from './app/navigators/TabNavigator';

export default function App() {
  return (
    <View style={styles.container}>
      <TabNavigator/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
  },
});
