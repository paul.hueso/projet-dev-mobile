class NewsService{
    getNews(){
        var url = 'https://newsapi.org/v2/everything?' +
        'q=covid&' +
        'sortBy=relevancy&' +
        'language=fr&'+
        'apiKey=c87d498b1d3c439f988f4895e65a028e';

        var req = new Request(url);

        return new Promise((resolve, reject) => {
            fetch(req)
            .then(httpResponse => httpResponse.json())
            .then(json => resolve(json))
            .catch(error => console.log(error));    
        })
    }
}  

export default NewsService;
